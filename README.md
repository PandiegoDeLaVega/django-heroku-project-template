## A project template for Django

To run on Heroku OR Gitlab's Auto DevOps (Herokuish)

### Usage

```
$ mkvirtualenv -p python3 my_project
$ pip install django
$ django-admin startproject --template https://gitlab.com/moutons-noirs/django-heroku-project-template/-/archive/master/django-heroku-project-template-master.zip -e json,txt,ini my_project
$ cd my_project
$ # FOR LOCAL DEVELOPMENT
$ echo "SECRET_KEY=`cat /dev/urandom | tr -dc '[:alnum:][:punct:]' | fold -w 128 | head -n 1`" >> .env
```

### Features

 * Python 3.7.x
 * Django settings powered by `django-decouple`
 * USWGI application container
 * Uses `dj-database-url` to configure databases on Heroku or Gitlab Auto DevOps

### Environment Variables

These are the available ENVVARS:
 * SECRET_KEY - *required* : a long random string, required in all environements (see `Usage` for a command to generate such a key)
 * DEBUG - *optional* (default `0`) : set Django in DEBUG mode
 * LOCAL - *optional* (default `0`) : use a SQLite database for local dev.
 * ALLOWED_HOSTS - *optional* (default `localhost,127.0.0.1`) : Comma-separated list of allowed hosts
 * K8S_PROMETHEUS - *optional* (default `0`) : For Gitlab, add internal (in-cluster) host address to ALLOWED_HOST to let Prometheus do its job

#### Heroku

Non-sensitive envvars can be set in the `app.json` file. The sensitive ones have to be set via the GUI or the `heroku` command line.

You MUST define :
 * `SECRET_KEY` or the application will not start.
 * `ALLOWED_HOSTS` or the application will not respond. For review apps to work, 
    make sure you define the ALLOWED_HOSTS envvar to `.herokuapp.com`.
    If you have a domain name add it too : `.herokuapp.com,.my-domain.com`.

### Gitlab Auto Devops

Non-sensitive envvars can be set through the [.gitlab.yml](https://gitlab.com/help/topics/autodevops/index.md#customizing-gitlab-ciyml) file. 
The senstive ones also have to be set through the GUI (`Settings->CI/CD->Variables`) AND PREFIXED with `K8S_SECRET_` so that they are available
in the application container! The `kubectl` tool is also useable but changes done with it won't be reflected in the Gitlab UI.

You MUST define (`K8S_SECRET_K8S_` prefix included):
 * `K8S_SECRET_K8S_SECRET_KEY` or the application will not start.
 * `K8S_SECRET_K8S_ALLOWED_HOSTS` or the application will not respond and review apps will be disabled. See below.
 * `K8S_SECRET_K8S_PROMETHEUS=1` or the application will not start.

Although you don't need a domain you can configure GitLab to publish review apps under your domain (`Operations->Kubernetes->Base Domain`). 
Of course you'll need to edit your domain configuration at your domain provider. In this case make sure you define the ALLOWED_HOSTS envvar to `.my-domain.com`.
Have a read of [Gitlab's brilliantly messy documentation](https://gitlab.com/help/user/project/clusters/index#base-domain).

### Tracking upstream Django project template

We are using `git read-tree`. To understand this beast, read and re-read this [Git Manual Chapter](https://git-scm.com/book/en/v1/Git-Tools-Subtree-Merging). Then fail, try again, and eventually succeed :).
Note: I think I screwed up.

